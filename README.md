# CKA study guide - k8s v1.28


BEFORE BEGINNING EXAM -> k8s docs: cheatsheet:
echo "source <(kubectl completion bash)" >> ~/.bashrc
export do="--dry-run=client -o yaml"
alias k="kubectl"
complete -o default -F __start_kubectl k


during exam:
best as fast as possible, but be exact (answers are computer rated)
proof-check if it works
keep reading while resources are being created



# TODO
practice kubectl output formatting with -o jsonpath={}



https://www.youtube.com/watch?v=VH1LRG1Ul2w&list=PLfeid9nyFRy8aHaoR_Nczw3tHj0Xf4fMY
https://kubernetes.io/docs/reference/kubectl/cheatsheet/

Tip:
- don't use "k edit" -> editor is clunky and laggy,
better to just
k get pod ... -oyaml > file, delete unnecessary stuff
and then k apply -f
- don't spend too much time on a question, answer as good as you can
to get the partial marks, that's good enough
- k api-resources  # list all api resources
- Can separate multiple yaml resources with --- in the same file
- Skip every hard question
- create imperative resources:
- kubectl -h
- kubectl explain pod.spec
- ps aux | grep kubelet   (to get conf location)
- k apply -f . (always apply yaml, also to create objects, this creates last-applied annotation so resources can be easily updated)
- k explain pv.spec | less -S
- k explain <kind>.spec | less -S
- k run --image=busybox --command sleep 5 # no = in command param at end!
- systemctl status kubelet
- systemctl status containerd
- cd /etc/kubernetes/manifests  # cluster config
- k replace -f <file.yaml> --force  # reconfigure a resource
- export dry="--dry-run=client -o yaml"
```


# static pods & resources
# node-specific, defined in:
/etc/kubernetes/manifests
# get this value: cat /var/lib/kubelet/config.yaml | grep staticPodPath
# static pods are easily identified by kubectl get pods -> no numerical suffix:
<podname>-<clustername>-<nodename>
# scheduled by kube-scheduler, delete kube-scheduler to reschedule


# statefulsets
k get statefulsets
# a form of 'sticky' deployment, that fixes the order
# named with suffix numbers: app-1 app-2 app-*

# scale statefulset or deployment
k -n my-ns scale --replicas=0 statefulset my-app


# normal pods from deployment
# named with standard suffix e.g. coredns-5d78c9869d-65pjz
# are not ordered unlike statefulsets



# liveness/readiness
# on container level in manifest
# use string for command
...
containers:
  - name: my-container
    livenessProbe:
      exec:
        command:
        - /bin/sh
        - -c
        - 'true'


# static pods
# -> manually schedule a pod on a specific node
# also works to schedule pods on master node
kind: Pod
spec:
  nodeName: ...


# k edit won't work
error: pods "manual-schedule" is invalid
A copy of your changes has been stored to "/tmp/kubectl-edit-3272075614.yaml"
kubectl replace --force -f /tmp/kubectl-edit-3272075614.yaml


# annotate a change e.g. updated or scaled deploy
https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
kubectl annotate deployment <deploy-name> kubernetes.io/change-cause="image updated to 1.16.1"



# metrics and resource usage - requires metrics server installed
k top pods
k top nodes


# get resource in ALL namespaces
k get pods -A

# schedule a pod on master node
master is tainted -> k describe <master-node>, add a toleration:
https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/

next, assign pod to node (can be via label or nodename -> only action needed to schedule on master, no need to add toleration!):
https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes/



# include a command for busybox
k run pod01 --image=busybox --command sleep 4800

kubectl create ns my-ns
kubectl run pod-name --image=alpine --namespace=my-ns --labels=my-key=my-value --dry-run=client -oyaml > 3_pod.yaml
kubectl apply -f 3_pod.yaml

k get pods --show-labels

kubectl create
  clusterrole           
  clusterrolebinding   
  configmap             cm
  cronjob               cj
  deployment            deploy
  ingress             
  job                   
  namespace             ns
  poddisruptionbudget   
  priorityclass         
  quota                 
  role                  
  rolebinding           
  secret                
  service               svc
  serviceaccount        sa
  token                 
```

## 25% - Cluster Architecture, Installation & Configuration

• Manage role based access control (RBAC)
user|group|sa + role + rolebinding = RBAC
rolebinding is namespaced, clusterrolebinding is for all namespaces

https://kubernetes.io/docs/reference/access-authn-authz/rbac/
kube-apiserver --authorization-mode=RBAC.. # configure startup in /etc/kubernetes/manifests
```
# create a namespace
kubectl create namespace my-namespace

# configure sa account
k create sa -h
# OR configure user
openssl genrsa -out john.key 2048
openssl req -new -key john.key -out john.csr -subj "/CN=john/O=bitnami"
openssl x509 -req -in john.csr -CA CA_LOCATION/ca.crt -CAkey CA_LOCATION/ca.key -CAcreateserial -out john.crt -days 500
# add the context for the john
kubectl config set-credentials john --client-certificate=/home/john/.certs/john.crt  --client-key=/home/john/.certs/john.key
kubectl config set-context john-context --cluster=<cluster-name> --namespace=<my-namespace> --user=john
kubectl --context=employee-context get pods



# Manage SAs with roles and then binding the account to the role
# with a role binding

# create role 
k create role -n my-namespace -h
k create clusterrole -n my-namespace -h
# e.g.
k create role processor --resource=secret,configmap --verb=create -n my-namespace

# bind the user to the role 
k create rolebinding -n my-namespace -h
k create clusterrolebinding -n my-namespace -h

kubectl create rolebinding my-sa-view \
  --clusterrole=view \
  --serviceaccount=my-namespace:my-sa \
  -n my-namespace
```


• Use Kubeadm to install a basic cluster 

- become root

https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

Prereq:
### configure /etc/hosts
### swapoff -a
### uncomment swap lines in /etc/fstab
```
- Install container runtime, apply network policy:
https://kubernetes.io/docs/setup/production-environment/container-runtimes/
- Configure cgroups driver, autoconfigures and defaults to systemd since v1.28.
- Install and configure container runtime:
apt install docker.io
systemctl start docker
systemctl enable docker
systemctl daemon-reload
systemctl start kubelet

# install kubeadm kubelet kubectl
sudo kubeadm init

kubeadm init --apiserver-advertise-address=172.10.10.10 --pod-network-cidr=10.0.0.0/8

Note: pod cidr and node IP's cannot overlap, or the network and pod creation will be unstable. Cilium install uses 10.0.0.0/8 as default, that's the entire 10.* private range used up.

# apply pod network [search cilium in docs]
cilium install

# join nodes
kubeadm token create -h
kubeadm token create --print-join-command  # on cp

# extra cp join, only for HA cluster
sudo kubeadm join <control-plane-host>:<control-plane-port> \
-- token <token> --discovery-token-ca-cert-hash sha256:<hash> \
-- control-plane --certificate-key <certificate-decryption-key>

# worker node join
sudo kubeadm join <control-plane-host>:<control-plane-port> \
--token <token> --discovery-token-ca-cert sha256:<hash>
```

• Manage a highly-available Kubernetes cluster
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/


• Provision underlying infrastructure to deploy a Kubernetes cluster
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
- kubeadm
- kubectl
- kubelet

### adding a node
```
# on worker node
swapoff -a
# ensure kubeadm kubectl kubelet is installed

# get join command via control-plane node
kubeadm token create --print-join-command
```


• Perform a version upgrade on a Kubernetes cluster using Kubeadm
# search: upgrade kubeadm
# the only difference is in 1st cp and all other nodes
# when executing kubeadm upgrade apply
# versions of kubeadm / kubelet / kubectl should match
https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/
```
# workflow: single cp, then all other cp's and nodes

# ensure swap disabled
swapoff -a

# update kubeadm
apt update
apt-cache madison kubeadm
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm='1.28.x-*' && \
apt-mark hold kubeadm

# upgrade kubeadm
kubeadm version
## if first cp
kubeadm upgrade plan
sudo kubeadm upgrade apply v1.28.x
## else
sudo kubeadm upgrade node

# drain the node
kubectl drain <node-to-drain> --ignore-daemonsets

# update kubelet and kubectl
ssh <node>
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet='1.28.x-*' kubectl='1.28.x-*' && \
apt-mark hold kubelet kubectl

# restart services
systemctl daemon-reload
systemctl restart kubelet

# uncordon the node
kubectl uncordon <node-to-uncordon>

# verify
kubectl get nodes

• Implement etcd backup and restore
https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/

```
ssh control-plane
ETCDCTL_API=3 ectdctl # check if etcdctl is installed - apt install etcd if not

# backup
# get facts
kubectl -n kube-system describe pod <etcd_pod>
# OR
cat /etc/kubernetes/manifests/etcd.yaml

# create snapshot - this command is in docs with all cert paths listed! 
# don't look for cert paths manually
ETCDCTL_API=3 etcdctl \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=<ca.crt> \
  --cert=<server.crt> \
  --key=<server.key> \
  snapshot save snapshot.db

# verify
ETCDCTL_API=3 etcdctl --write-out=table snapshot status snapshot.db

# restore - recommended to do at end of exam
# make sure you have a snapshot to restore to!!
grep data-dir /etc/kubernetes/manifests/etcd.yaml
ETCDCTL_API=3 etcdctl \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=<ca-file> \
  --cert=<cert-file> \
  --key=<key-file> \
  snapshot restore snapshot.db --data-dir=<data-dir>
vim /etc/kubernetes/manifests/etcd.yaml
# --data-dir=<data-dir>
# point to the newly created etcd --data-dir!
```

## 15% - Workloads & Scheduling

• Understand deployments and how to perform rolling update and rollbacks
https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
https://kubernetes.io/docs/reference/kubectl/cheatsheet/
```
kubectl set image deploy q5 nginx=nginx:latest --record=true
```

• Use ConfigMaps and Secrets to configure applications
https://kubernetes.io/docs/concepts/configuration/configmap/
https://kubernetes.io/docs/concepts/configuration/secret/

## cm used in pod
```
spec:
  containers:
  - image: alpine
    name: xy
    envFrom:
      - configMapRef:
          name: my-cm
```


• Know how to scale applications
https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/

# to create replicaset fast:
k create deploy <name> --image=<image> $dry > yaml
# then delete strategy

# to create daemonset fast:
k create deploy <name> --image=<image> $dry > yaml
# then delete strategy, replicas

# deploy, rs, svc work the same -> resources are selected for labels
# and check if they match


https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
create a pod autoscaler:
```
k autoscale -h
```
each deploy manages a ReplicaSet


• Understand the primitives used to create robust, self-healing, application deployments
https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/
- ReplicaSets
- CronJobs
- DaemonSets

# DaemonSets -> create a deploy and edit
kind: DaemonSet
k create deploy --dry-run=client -oyaml > ..
# to run DaemonSets on cp node -> look for DaemonSets in docs and 
# copy paste the toleration

```
# creating a static pod
https://kubernetes.io/docs/tasks/configure-pod-container/static-pod/

systemctl status kubelet
vim /var/lib/kubelet/config.yaml

[+]    staticPodPath: /etc/kubernetes/manifests

# create yaml and store in /etc/kubernetes/manifests:
apiVersion: v1
kind: Pod
metadata:
  name: static-web
  labels:
    role: myrole
spec:
  containers:
    - name: web
      image: nginx
      ports:
        - name: web
          containerPort: 80
          protocol: TCP

systemctl restart kubelet
```


• Understand how resource limits can affect Pod scheduling
https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/


• Awareness of manifest management and common templating tools
https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/
https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/
https://kubernetes.io/docs/tasks/manage-kubernetes-objects/imperative-command/
helm



## 20% - Services & Networking

• Understand host networking configuration on the cluster nodes
https://kubernetes.io/docs/concepts/services-networking/
- podnameselector?
- namespaceselector?
- expose a deploy?

Create and expose a pod with clusterIP service
```
k run test1 --image=busybox:1.28 --expose=true --port=2828
```


• Understand connectivity between Pods
https://kubernetes.io/docs/concepts/services-networking/
https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/
https://kubernetes.io/docs/concepts/services-networking/network-policies/


• Understand ClusterIP, NodePort, LoadBalancer service types and endpoints
https://kubernetes.io/docs/concepts/services-networking/
https://kubernetes.io/docs/concepts/services-networking/service/


• Know how to use Ingress controllers and Ingress resources
https://kubernetes.io/docs/concepts/services-networking/ingress/
https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/


• Know how to configure and use CoreDNS
https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-reconfigure/
kubectl edit deployment -n kube-system coredns
kubectl edit service -n kube-system kube-dns


• Choose an appropriate container network interface plugin
https://kubernetes.io/docs/tasks/administer-cluster/network-policy-provider/cilium-network-policy/



## 10% - Storage

Hostpath means local storage.
PV's are accessible by all namespaces.

• Understand storage classes, persistent volumes
# pod and pvc is namespaced!
https://kubernetes.io/docs/concepts/storage/
https://kubernetes.io/docs/concepts/storage/storage-classes/
https://kubernetes.io/docs/concepts/storage/persistent-volumes/


• Understand volume mode, access modes and reclaim policies for volumes
https://kubernetes.io/docs/tasks/administer-cluster/change-pv-reclaim-policy/
https://kubernetes.io/docs/concepts/storage/persistent-volumes/


• Understand persistent volume claims primitive
https://kubernetes.io/docs/concepts/storage/persistent-volumes/


• Know how to configure applications with persistent storage
https://kubernetes.io/docs/tasks/configure-pod-container/
https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/



## 30% - Troubleshooting



• Evaluate cluster and node logging
https://kubernetes.io/docs/tasks/debug/debug-cluster/
kubectl get events
journalctl -u kubelet
systemctl status
systemctl status kubelet
systemctl status containerd
vim /etc/systemd/system/kubelet.service.d/kubeadm.conf
systemctl daemon-reload
systemctl restart kubelet

• Understand how to monitor applications
https://kubernetes.io/docs/tasks/debug/debug-cluster/monitor-node-health/
kubectl top nodes
kubectl top pods

cat /etc/kubernetes/manifests/etcd.yaml
kubectl describe pods <etcd-pod> -n kube-system
ETCDCTL_API=3 etcdctl --endpoints <etcd-ip>:2379 \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  member list

• Manage container stdout & stderr logs
https://kubernetes.io/docs/tasks/debug/debug-cluster/
```
k logs my-pod                                 # dump pod logs (stdout)
k logs -l name=myLabel                        # dump pod logs, with label name=myLabel (stdout)
k logs my-pod --previous                      # dump pod logs (stdout) for a previous instantiation of a container
k logs my-pod -c my-container                 # dump pod container logs (stdout, multi-container case)
k logs -l name=myLabel -c my-container        # dump pod logs, with label name=myLabel (stdout)
k logs my-pod -c my-container --previous      # dump pod container logs (stdout, multi-container case) for a previous instantiation of a container
k logs -f my-pod                              # stream pod logs (stdout)
k logs -f my-pod -c my-container              # stream pod container logs (stdout, multi-container case)
k logs -f -l name=myLabel --all-containers    # stream all pods logs with label name=myLabel (stdout)
```

• Troubleshoot application failure
https://kubernetes.io/docs/tasks/debug/debug-application/
```
k get po -n kube-system

# debug pod
k describe pods ${POD_NAME}
k apply --validate -f mypod.yaml

# debug replicaset
k describe rc ${CONTROLLER_NAME}

# debug endpoints
k get ep ${SERVICE_NAME}

...
spec:
  - selector:
     name: nginx
     type: frontend

k get po --selector=name=nginx,type=frontend

Debug service:
k run -it --rm --restart=Never busybox --image=gcr.io/google-containers/busybox sh
k exec <POD-NAME> -c <CONTAINER-NAME> -- <COMMAND>
cat /etc/resolv.conf  # inside pod

kubectl <command> --help | grep kubectl
```


• Troubleshoot cluster component failure
https://kubernetes.io/docs/tasks/debug/debug-cluster/
```
kubectl version
kubectl config view
cat ~/.kube/config
cat /etc/kubernetes/admin.conf
cd /etc/kubernetes/manifests/
cd /var/log
crictl ps -a
```


• Troubleshoot networking
https://kubernetes.io/docs/tasks/debug/debug-cluster/
```
kubectl get nodes
kubectl debug no mynode -it --image=ubuntu # if no ssh access to node, creates pod

...
apt install dnsutils
nslookup <podname>
```





# temporarily stop kube-scheduler
```
cd /etc/kubernetes/manifests
mv kube-scheduler.yaml ~
```



# Only one pod per node needed?
# -> add a required pod anti-affinity so that pods don't get scheduled
spec:
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: my-pod-key
            operator: In
            values:
            - my-pod-value
        topologyKey: kubernetes.io/hostname

* Two common topologyKey options are topology.kubernetes.io/zone and kubernetes.io/hostname. 
    topology.kubernetes.io/zone: Pods will be scheduled in the same zone as a Pod that matches the expression.
    kubernetes.io/hostname: Pods will be scheduled on the same hostname as a Pod that matches the expression.

# Many other options with node and pod affinities ->
# required or preferred scheduling based on labels!


# expose pod environment info to containers:
search fieldref


# simple ephemeral volume podspec
spec:
  volumes:
  - name: my-volume
    emptyDir: {}
  containers:
  - image: nginx
    name: c1
    volumeMounts:
      - mountPath: /vol/path
        name: my-volume


# command in shell - are written on container level
...
  containers:
  - ...
    command: ["/bin/sh"]
    args: ["-c", "<paste long command here>"]


# troubleshoot a container in a pod
k exec <POD> <CONTAINER> -- <COMMAND>


# CIDR = classless interdomain routing
# used to define IP ranges pods are assigned to in cluster
ssh cp
grep service-cluster-ip-range /etc/kubernetes/manifests/kube-apiserver.yaml
# out: - --service-cluster-ip-range=10.96.0.0/16

# CNI = container network interface e.g. weave, calico
ls /etc/cni/net.d/



# get all events and sorted by json object
k get events -A --sort-by=.metadata.creationTimestamp



# kill containerd kube-proxy container on node:
crictl ps | grep kube-proxy
crictl rm -f cd196bb1ab8cd

# get extra info of container
crictl inspect 976d88244dae0 | grep runtime


# all namespaces api resources
k api-resources --namespaced=true


# count the number of a resource
k get role --no-headers -A | wc -l


# run pod with multiple labels
k run podname --image httpd:2.4.41-alpine --labels="key1=val1,key2=val2"


# kubelet service could not start, fix it:
ssh node
systemctl status kubelet
journalctl -xu kubelet       
# -x for verbose, -u to specify the service, check for exact error
# check for "Failed" status
vim /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
systemctl daemon-reload
systemctl start kubelet





# secrets
# create the secret
k create secret -h
k create secret generic --from-file=<path>
k create secret generic --from-literal=key1=val1


# mount a secret into a Pod
spec:
  volumes:
    - name: secret-volume
      secret:
        secretName: dotfile-secret
  containers:
    - name: ...
      volumeMounts:
        - name: secret-volume
          readOnly: true
          mountPath: "/etc/secret-volume"


# secret as env var:
containers:
  env:
    - name: MY_SECRET
      valueFrom:
        secretKeyRef:
          name:
          key:
          ...


# check if secret is defined
k exec <pod> <container> -- env





# expose a pod/deploy (i.e. service)
kubectl expose pod my-pod --type="NodePort" --port 80

# create ingress
kubectl create ingress simple --rule="foo.com/bar=svc1:8080,tls=my-cert"
, where
foo.com = domain name
/bar = path
sv1 = backend service
8080 =  backend service port
my-cert = name of secret resource containing tls information


# select everything with {}!
podSelector: {}



# managing certificates with kubeadm
kubeadm certs -h
kubeadm certs check-expiration
openssl x509 -noout -text -in /etc/kubernetes/pki/apiserver.crt
kubeadm certs renew ...



# all certs and startup defaults are in:
/etc/kubernetes/manifests/kube-apiserver.yaml



# defining label matches
# OPTION 1
matchLabels
  key1: val1
# OPTION 2 - advanced match with regex example
matchExpressions:
  - key: keyval1
  - operator: In|NotIn|Exists|DoesNotExist
  - values: ["db*"]


# network policy
ingress = allow traffic FROM something labelled into matched pod label
# for when traffic can only be received by a certain destination

egress = allow connection of a matched pod label TO other matched labelled pods
# for when traffic can only be sent to a certain destination 

# network policy AND
  ...
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          user: alice
      podSelector:
        matchLabels:
          role: client

# network policy OR
  ...
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          user: alice
    - podSelector:
        matchLabels:
          role: client

# troubleshoot network policy
k describe networkpolicy <name>
k exec -it <podname> -- nc -vz <IP> <PORT>
k exec -it <podname> -- timeout 2 nc -vz <IP> <PORT>
k exec -it <podname> -- wget --spider --timeout=1 <IP>



# config files
/etc/kubernetes

# kubelet config
/etc/kubernetes/kubelet.conf
# kubectl config
/etc/kubernetes/admin.conf
# controller config
/etc/kubernetes/controller-manager.conf
# scheduler config
/etc/kubernetes/scheduler.conf

# yaml files for static pods and kube-system pods
/etc/kubernetes/manifests




# kubelet certs
root@control-plane:/var/lib/kubelet/pki
.
# client key to communicate with api-server
|-- kubelet-client-current.pem
# server key to communicate with api-server
|-- kubelet.crt
# server key to communicate with api-server
`-- kubelet.key





# helm basics

# list releases
helm list -a

# install a release
helm install releasename release-folder/

# create a release
helm create releasename

# update a helm repo and apply the changes
helm upgrade releasename release-folder/



# json query
# first find out what items you need
k get ... -o json
# find path easier, just flatten all and grep it:
k get ... -o json | jq -c 'paths|join(".")' | grep memory

# then create the query e.g.
# get name and namespaces of all pods
# {range .items[*]} -> iterates over list of all items
kubectl get pods -A -o jsonpath='{range .items[*]}{.metadata.namespace}{"\t"}{.metadata.name}{"\n"}{end}'

# you can also use custom-columns
# get available memory/cpu
k get nodes -o custom-columns=NAME:.metadata.name,AVAILABLE_MEMORY:.status.allocatable.memory,AVAILABLE_CPU:.status.allocatable.cpu

# or query a column for a specific value:
# [?(@.key == "value-string")]
# ? = filter expression
# @ = placeholder for current element
jsonpath='{.users[?(@.name == "e2e")].user.password}'



# allow container ability to change system time:
search: security context 
SYS_TIME




# general troubleshoot
# always double-check if pods are running from easy questions
# if not...
k get pods -n kube-system
k describe pod ... -n kube-system
# then depending on error
systemctl status ...
# or
cd /etc/kubernetes/manifests



# can't connect to nodes? api-server is misconfigured!
# CHECK LOGS:
/var/log/pods
/var/log/containers
crictl ps
crictl logs
(docker ps + docker logs if docker used)
journalctl | grep apiserver

vim /etc/kubernetes/manifests/kube-apiserver.yaml

check docs for proper config:
https://kubernetes.io/docs/reference/command-line-tools-reference/







# stop scheduling on a node - works for all scheduling
# including daemonsets
k taint no foo bar:NoSchedule
# then remove the taint
k taint no foo bar-

# OR
k cordon NODE
k uncordon NODE





# Volumes in Pod with init container
spec:
  containers:
  - name: ...
    ...
    volumeMounts:
    - name: mydata
      mountPath: /usr/share/index.html
  initContainers:
  - name: ...
    ...
    volumeMounts:
    - name: mydata
      mountPath: /data
    args:
      - touch
      - index.html
  volumes:
  - name: mydata
    emptyDir: {}





# Services expose an app
k get svc
k expose deploy/pod ... -h
k get ep

# Container --> Service --> ClusterIP


# Ingress
# Ingress rules route traffic to services
# Require an ingress controller (e.g. nginx) to work
k get ingress
k create ingress -h

